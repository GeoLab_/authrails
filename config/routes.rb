Rails.application.routes.draw do
  get 'home/index'

  devise_for :users

  get 'home/private'

  root to: "home#index"
end
